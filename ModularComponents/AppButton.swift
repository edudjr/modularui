//
//  AppButton.swift
//  ModularComponents
//
//  Created by Eduardo Domene Junior on 21.12.20.
//

import UIKit

struct AppButtonViewModel {
    let mobilityType: MobilityType
}

protocol AppButtonDelegate: class {
    func tapped(_ mobilityType: MobilityType)
}

class AppButton: UIButton {
    let vm: AppButtonViewModel
    weak var delegate: AppButtonDelegate?
    
    init(vm: AppButtonViewModel) {
        self.vm = vm
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setTitle(vm.mobilityType.rawValue, for: .normal)
        self.setTitleColor(.gray, for: .normal)
        self.translatesAutoresizingMaskIntoConstraints = false
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(tap))
        self.addGestureRecognizer(tapGesture)
    }
    
    @objc func tap() {
        delegate?.tapped(vm.mobilityType)
    }
}
