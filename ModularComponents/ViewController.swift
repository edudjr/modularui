//
//  ViewController.swift
//  ModularComponents
//
//  Created by Eduardo Domene Junior on 21.12.20.
//

import UIKit
import Swinject

enum MobilityType: String {
    case scooter, carsharing
}

class ViewController: UIViewController, AppButtonDelegate {
    var factory: DetailsViewControllerFactory
    
    lazy var scooterButton: UIButton = {
        let vm = AppButtonViewModel(mobilityType: .scooter)
        let vc = AppButton(vm: vm)
        vc.delegate = self
        return vc
    }()
    
    lazy var carSharingButton: UIButton = {
        let vm = AppButtonViewModel(mobilityType: .carsharing)
        let vc = AppButton(vm: vm)
        vc.delegate = self
        return vc
    }()
    
    init(factory: DetailsViewControllerFactory) {
        self.factory = factory
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        view.addSubview(scooterButton)
        view.addSubview(carSharingButton)
        
        view.backgroundColor = .white
        
        scooterButton.centerX().centerY()
        scooterButton.centerX().centerY()
        
        carSharingButton.centerX()
        carSharingButton.centerYAnchor.constraint(equalTo: scooterButton.centerYAnchor,
                                                  constant: 40.0).isActive = true
    }
    
    // MARK: AppButtonDelegate
    func tapped(_ mobilityType: MobilityType) {
        present(factory.makeViewController(for: mobilityType), animated: true)
    }
}

class CarSharingVehicleDetails: UIView {
    let stack: UIStackView = {
        let stack = UIStackView()
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    let fuel: UILabel = {
        let label = UILabel()
        label.text = "Fuel"
        label.backgroundColor = .cyan
        return label
    }()
    let pricePerKM: UILabel = {
        let label = UILabel()
        label.text = "$ KM"
        label.backgroundColor = .orange
        return label
    }()
    override func layoutSubviews() {
        super.layoutSubviews()
        addSubview(stack)
        stack.axis = .horizontal
        stack.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        stack.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        stack.addArrangedSubview(fuel)
        stack.addArrangedSubview(pricePerKM)
    }
}

class ScooterVehicleDetails: UIView {
    let stack: UIStackView = {
        let stack = UIStackView()
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    let battery: UILabel = {
        let label = UILabel()
        label.text = "Battery"
        label.backgroundColor = .cyan
        return label
    }()
    let unlockPrice: UILabel = {
        let label = UILabel()
        label.text = "Unlock"
        label.backgroundColor = .green
        return label
    }()
    let pricePerMinute: UILabel = {
        let label = UILabel()
        label.text = "$ Minute"
        label.backgroundColor = .orange
        return label
    }()
    override func layoutSubviews() {
        super.layoutSubviews()
        addSubview(stack)
        stack.axis = .horizontal
        stack.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        stack.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        stack.addArrangedSubview(battery)
        stack.addArrangedSubview(unlockPrice)
        stack.addArrangedSubview(pricePerMinute)
    }
}

class DetailsViewController: UIViewController {
    let stack: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.alignment = .center
        return stack
    }()
    let vehicleImage: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 82)
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    init(vehicleIcon: String, vehicleDetails: UIView) {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
        view.addSubview(stack)
        stack.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        stack.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        stack.addArrangedSubview(vehicleImage)
        stack.addArrangedSubview(vehicleDetails)
        vehicleImage.widthAnchor.constraint(equalToConstant: 140).isActive = true
        vehicleImage.text = vehicleIcon
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class DetailsViewControllerFactory {
    let container: Container
    
    init(container: Container) {
        self.container = container
    }
    
    func makeViewController(for mobilityType: MobilityType) -> UIViewController {
        container.resolve(DetailsViewController.self, name: mobilityType.rawValue)!
    }
}

extension UIView {
    @discardableResult
    func centerX() -> UIView {
        guard let superview = superview else { return self }
        self.centerXAnchor.constraint(equalTo: superview.centerXAnchor).isActive = true
        return self
    }
    
    @discardableResult
    func centerY() -> UIView {
        guard let superview = superview else { return self }
        self.centerYAnchor.constraint(equalTo: superview.centerYAnchor).isActive = true
        return self
    }
}
